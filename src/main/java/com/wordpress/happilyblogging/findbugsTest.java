package com.wordpress.happilyblogging;

public class findbugsTest {
    
    private String name;

    public boolean equals(Object o) {
        if (o instanceof Foo) {
            return name.equals(((Foo) o).name);
        } else if (o instanceof String) {
            return name.equals(o);
        } else {
            return false;
        }
    }
}

class Foo{
    public String name;
}
